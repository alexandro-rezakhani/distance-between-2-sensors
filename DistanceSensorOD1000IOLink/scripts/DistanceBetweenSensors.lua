--[[----------------------------------------------------------------------------

  Application Name:
  DistanceBetweenSensors

  Summary:
  Connecting and communicating to two IO-Link distance sensors

  Description:
  This sample shows how to connect to the IO-Link device OD1000 and how to
  receive distance measurement data.

  How to run:
  This sample can be run on any AppSpace device which can act as an IO-Link master,
  e.g. SIM family. The IO-Link device OD1000 must be properly connected to a port
  which supports IO-Link. If the port is configured as IO-Link master, see script,
  the power LED blinks slowly. When a IO-Link device is successfully connected the
  LED blinks rapidly.

  More Information:
  See device manual of IO-Link master for according ports. See manual of IO-Link
  device OD1000 for further IO-Link specific description and device specific commands.

------------------------------------------------------------------------------]]
--Start of Global Scope---------------------------------------------------------

require('ioLink')

-- Creating timer to cyclicly read process data of OD1000 device
gTmrStatus = {false, false}
gTmr = Timer.create()
gTmr:setExpirationTime(100)
gTmr:setPeriodic(true)

--End of Global Scope-----------------------------------------------------------

--Start of Function and Event Scope---------------------------------------------

-- On every expiration of the timer, the process data of IO-Link device OD1000 is read
--@handleOnExpired()
local function handleOnExpired()
  -- Reading process data on port 1
  local data = gIoLinkDevice1:readProcessData()
  -- Extracting distance value out of process data
  local distance1 = string.unpack('i2', data)
  -- Reading process data on port 2
  data = gIoLinkDevice2:readProcessData()
  -- Extracting distance value out of process data
  local distance2 = string.unpack('i2', data)

  local distance = distance2 - distance1
  print("The distance between the sensors is "..tostring(distance).." mm")
end
Timer.register(gTmr, 'OnExpired', handleOnExpired)

--End of Function and Event Scope-----------------------------------------------
