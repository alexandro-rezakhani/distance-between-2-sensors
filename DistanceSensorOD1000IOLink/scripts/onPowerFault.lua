--@handleOnPowerFault()
local function handleOnPowerFault()
  if port == 1 then
  print('Power fault at IO-Link device on port 1')
  gTmrStatus[1] = false
  elseif port == 2 then
  print('Power fault at IO-Link device on port 2')
  gTmrStatus[2] = false
  end
  if gTmr:isRunning() then gTmr:stop() end
end

return handleOnPowerFault