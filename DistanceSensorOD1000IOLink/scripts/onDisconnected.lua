-- Stopping timer when IO-Link device gets disconnected
--@handleOnDisconnected()
local function handleOnDisconnected()
  if port == 1 then
  print('IO-Link device disconnected to port 1')
  gTmrStatus[1] = false
  elseif port == 2 then
  print('IO-Link device disconnected to port 2')
  gTmrStatus[2] = false
  end
  if gTmr:isRunning() then gTmr:stop() end
end

return handleOnDisconnected