--@handleOnConnected()
local function handleOnConnected(port)
  if port == 1 then
  print('IO-Link device connected to port 1')
  gTmrStatus[1] = true
  elseif port == 2 then
  print('IO-Link device connected to port 2')
  gTmrStatus[2] = true
  end
  --Starting timer after successfull connection
  if gTmrStatus[1] and gTmrStatus[2] then
    if not gTmr:isRunning() then gTmr:start() end
  end
end

return handleOnConnected