-- Enable power on S1 port, must be adapted if another port is used
-- luacheck: globals gPwr gIoLinkDevice1 gIoLinkDevice2 gTmr
gPwr1 = Connector.Power.create('S1')
gPwr1:enable(true)
gPwr2 = Connector.Power.create('S2')
gPwr2:enable(true)

fns = {}
fns.handleOnConnected = require('onConnection')
fns.handleOnDisconnected = require('onDisconnected')
fns.handleOnPowerFault = require('onPowerFault')

-- Creating IO-Link device handle for S1 port, must be adapted if another port is used
-- Now S1 port is configured as an IO-Link master.
gIoLinkDevice1 = IOLink.RemoteDevice.create('S1')
gIoLinkDevice2 = IOLink.RemoteDevice.create('S2')

local function handleOnConnected1()
  fns.handleOnConnected(1)
end
IOLink.RemoteDevice.register(gIoLinkDevice1, 'OnConnected', handleOnConnected1)
local function handleOnDisconnected1()
  fns.handleOnDisconnected(1)
end
IOLink.RemoteDevice.register( gIoLinkDevice1, 'OnDisconnected', handleOnDisconnected1)
local function handleOnPowerFault1()
  fns.handleOnPowerFault(1)
end
IOLink.RemoteDevice.register(gIoLinkDevice1, 'OnPowerFault', handleOnPowerFault1)

local function handleOnConnected2()
  fns.handleOnConnected(2)
end
IOLink.RemoteDevice.register(gIoLinkDevice2, 'OnConnected', handleOnConnected2)
local function handleOnDisconnected2()
  fns.handleOnDisconnected(2)
end
IOLink.RemoteDevice.register( gIoLinkDevice2, 'OnDisconnected', handleOnDisconnected2)
local function handleOnPowerFault2()
  fns.handleOnPowerFault(2)
end
IOLink.RemoteDevice.register(gIoLinkDevice2, 'OnPowerFault', handleOnPowerFault2)
